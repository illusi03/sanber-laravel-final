<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
  use AuthenticatesUsers;
  protected function redirectTo() {
    $user = Auth::user();
    if ($user->account_type == 1) {
      return '/';
    } else {
      return '/login';
    }
  }
  protected $redirectTo = '/';
  public function __construct() {
    $this->middleware('guest')->except('logout');
  }
}
